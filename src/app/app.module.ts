import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApplicationlistComponent } from './applicationlist/applicationlist.component';
import { FormComponent } from './form/form.component';
import { RouterModule, Routes } from "@angular/router";



const routes: Routes = [
  {path: 'applicationlist', component:ApplicationlistComponent},
  {path: 'form', component: FormComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ApplicationlistComponent,
    FormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
